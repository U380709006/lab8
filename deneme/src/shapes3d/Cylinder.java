package shapes3d;
import shapes2d.Circle;

public class Cylinder extends Circle {
    private double length;

    public Cylinder(double r, double l) {
        super(r);
        length = l;
    }

    public double getLength() {
        return length;
    }

    public double findArea() {

        return 2*super.findArea()
                + findCircumference()*length;
    }

    public double findVolume() {
        return super.findArea()*length;
    }

}