package shapes3d;
import shapes2d.Square;

public class Cube extends Square {
    private int depth = 6;

    Cube(double height, double width) {
        super(height, width);
    }

    public double computeSurfaceArea() {
        super.surfaceArea = (super.height * super.width) * depth;
        return super.surfaceArea;
    }
}
