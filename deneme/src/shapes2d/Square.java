package shapes2d;

public class Square {
    public double height;
    public double width;
    public double surfaceArea;


    public Square(double height, double width) {
        this.height = height;
        this.width = width;
    }


    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return this.width;
    }

    public double computeSurfaceArea() {
        this.surfaceArea = this.height * this.width;
        return this.surfaceArea;
    }

    public double getSurfaceArea() {
        return this.surfaceArea;
    }

}