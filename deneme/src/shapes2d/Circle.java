package shapes2d;

public class Circle {

    private double radius;


    public Circle(double r) { radius = r; }

    public double getRadius() { return radius; }

    public double findCircumference() {
        return 2*Math.PI*radius;
    }

    public double findArea() {
        return radius*radius*Math.PI;
    }
}